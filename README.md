> :warning: **This Delivery kit has been Archived and nested within [Implementation Delivery Kit](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/implementation-delivery-kits/implementation-delivery-kit)** :warning:
# Implementation QuickStart - GitLab.com

# Overview
This is a pre-packaged engagement that is focused on getting a gitlab.com customer up and running quickly from the perspective of identity/access management, and group-level security configurations. The customer-facing package details, including the data sheet and service description are posted on our [public catalog](https://about.gitlab.com/services/catalog). 

The deliverable for this service is a solution roadmap. You can find a [TEMPLATE of that document here](https://docs.google.com/document/d/1LVf8vEzwe0pZy1iEyRppLezgAhgto-9jVWPaEJFiwtU/edit?usp=sharing) - Make sure to make a copy of this template. Please contribute sections that you had to elaborate on for the customer back to the template by using suggesting mode.  

# Checklist

## Project Kickoff
Please review the [service description](https://about.gitlab.com/services/skus/implementation-quick-start-com/) and [data sheet](https://drive.google.com/file/d/1dtCtZjCgolbO0lZ8BzTfPG_BNC6Tv078/view) to get an understanding of this service.

## Namespace and License
> While this should have already been taken care of by Sales, it's not a difficult process and can help get the engagement started. Reach out on slack to #support_gitlab-com if you hit a roadblock.
https://docs.gitlab.com/ee/subscriptions/index.html
1) Create or Reclaim a Namespace (https://handbook.gitlab.com/handbook/support/internal-support/#regarding-gitlab-support-plans-and-namespaces)
2) Have the Customer sign in to the portal (https://customers.gitlab.com/customers/sign_in). Whoever purchased gitlab will have these at the organization.
3) Associate the customers account to the namespace https://docs.gitlab.com/ee/subscriptions/index.html#change-the-associated-namespace 
### Links
- https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#co-terming
- https://about.gitlab.com/handbook/sales/field-operations/order-processing/#post-sale-information
- https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/
- https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/saas/associate_subscription_and_namespace/#customer-self-serve-associating-the-subscription-and-namespace

## Single-sign on
Demo Video: https://www.youtube.com/watch?v=0ES9HsZq0AQ&feature=youtu.be
1) Group [Single-sign on](https://docs.gitlab.com/ee/user/group/saml_sso/index.html) must be configured
2) Later, [SCIM provisioning](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html#gitlab-configuration)

## IP Whitelisting
To restrict [group IP Address](https://gitlab.com/gitlab-org/gitlab/-/issues/1985).
**NOTE** [Group IP restriction improvements](https://gitlab.com/gitlab-org/gitlab/-/issues/12195)

## Group owners
1) Refer [group member permission](https://docs.gitlab.com/ee/user/permissions.html#group-members-permissions) to understand various user permission settings
2) Refer [to change permssion to owners](https://docs.gitlab.com/ee/user/group/#changing-the-owner-of-a-group)
3) Setup Group Sync for SAML Integration https://docs.gitlab.com/ee/user/group/saml_sso/#group-sync
## 2FA settings
Refer [to enforce 2FA for all users](https://docs.gitlab.com/ee/security/two_factor_authentication.html#enforcing-2fa-for-all-users-in-a-group).
Refer [to setup 2FA](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#enabling-2fa)

## Gitlab-Runners
1) There are different ways of installing [gitlab-runner](https://docs.gitlab.com/runner/install/index.html)
2) This is how we can [configure a runner](https://docs.gitlab.com/runner/configuration/)
3) Lastly, [registering runner](https://docs.gitlab.com/runner/register/)

## Migration Assessment
> :warning: there are no migration services included in this. The deliverable is a recommendation!
- [ ] Ask how many projects, groups and users are in scope for migration. 
- [ ] Ask how many large repos (over 5GB) they have to migrated
- [ ] Ask the average size (disk space) of registries in scope for migration. 

- Alternatively, have the Customer run [Evaluate](https://gitlab.com/gitlab-org/professional-services-automation/tools/utilities/evaluate) on their instance to get an estimate of the migration effort. Pro-tip: have them run it in the docker container to minimize dependency errors. 

Based on the answers to :point_up:, recommend migration approach. 

See [migration talking points](migration_outline.md) for a checklist of discovery and topics to discuss.

_Edit this delivery kit by following our [contribution guidelines](/CONTRIBUTING.md)_

