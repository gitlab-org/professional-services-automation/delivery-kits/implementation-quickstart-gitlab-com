# Migration Discussion Checklist

- [ ] GitLab groups and projects - many source systems (Bitbucket, TFS) consider a "project" to be a collection of souce code repos; in GitLab, a project is a single source code repo and related tools and meta-data for collaboration around that repo (issues, wiki, merge requests, etc).  Use groups to group together repos that a team or set of teams should have access to.  
  - Note: groups are hierarchical and inherit permissions from the top down, projects inherit permissions from parent groups.  Doc: [subgroups](https://docs.gitlab.com/ee/user/group/subgroups/), [subgroup permissions](https://docs.gitlab.com/ee/user/permissions.html#subgroup-permissions)
  - Note: some/all of this might have been covered already in previous Rapid Results activities
- [ ] Git or non-Git source system?  Generally, Git repos can be imported with full history (with some caveats) to GitLab.  "Tip conversion", i.e. checkout current state of branches you will need then commit them to a new git repo, is recommended for non-Git source SCM systems.  You can keep your current systems read-only to view historical data as needed.
   - Note: for TFS repos, Microsoft recommends [git-tfs](https://github.com/git-tfs/git-tfs/blob/master/doc/usecases/migrate_tfs_to_git.md), open sourced on github.com to convert repos to git format
      - TO-DO: capture "gotchas" for conversion of TFS to git
- [ ] importing git repos: there are several ui based options for importing projects.  See [documentation](https://docs.gitlab.com/ee/user/project/import/) for details
   - Note: for imports from [GitHub](https://docs.gitlab.com/ee/user/project/import/github.html) or [Bitbucket](https://docs.gitlab.com/ee/user/project/import/bitbucket_server.html), additional meta data is brought over with the project import
- [ ] Importing issues -  Issues can be imported via the ui or api.  Some notes about GitLab issues:
   - no issue types - in GitLab an issue is an issue.  We recommend using [scoped labels](https://docs.gitlab.com/ee/user/project/labels.html) to define type.
   - no custom fields - many source systems allow you to define custom fields.  In GitLab, we recommend using issue templates with instructions for what information to fill out embedded in the template, and/or labels to track information you care about.  Often times, customers will export existing issues to Excel/csv, then massage the data from custom fields to format it into an issue description that they want to use in GitLab going forward. That data can then be imported into GitLab via the ui or using our apis.
   - no custom workflow - GitLab issues are either open or closed.  If you desire intermediate workflow states, we recommend using scoped labels and project and/or group level boards for managing your workflow
- [ ] Importing Registries to GitLab (container registry/package repos)
   - TO-DO: find doc and best practices
- [ ]  Importing other object types - the types of objects and meta-data that exist varies widely from one source system to the next.  What objects are important to you in your source system?  How are these used today? Let's map those objects to GitLab functionality:
   - TO-DO ... what data is important here?  functionality will generally map to test cases (caution: minimal GitLab functionality), releases, milestones, epics
   - Note: [GitLab apis](https://docs.gitlab.com/ee/api/) can be used to script import of many objects.
   - TO-DO: what scripts already exist in places like [gitlab-cs-tools](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools) to help with this?
- [ ] Migrating CI/CD jobs is outside of the scope of this engagement.  But the CI/CD training that is part of the Rapid Results will give you a good starting point for CI concepts in GitLab. We also have a lot of additional resources to help you get started with GitLab CI and translate existing CI processes to GitLab CI.  See:
   - [Auto Devops Documentation](https://docs.gitlab.com/ee/topics/autodevops/)
   - [Getting Started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/)
   - [GitLAb CI/CD Keyword Reference](https://docs.gitlab.com/ee/ci/yaml/)
   - [GitLab Learn - Continuous Integration](https://about.gitlab.com/learn/topics/continuous-integration/)
   - [GitLab Learn - Full list of topics](https://about.gitlab.com/learn/topics/)
